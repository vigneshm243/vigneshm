+++ 
date = 2021-12-18T08:56:47Z
title = "Spring in depth 1"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Java", "Spring"]
categories = ["Spring"]
externalLink = ""
series = ["Spring"]
+++

# Spring in Depth 1

## Auto wiring 

Auto wiring looks for a dependency that implements the interface that is auto-wired. Let's just look at a sample case below

```java
@Component
public class ClientApp{
    @Autowired
    Service reqService;
}
```
```java
@Component
public class SplService implements Service{
}
```
```java
@Component
public class NormService implements Service{
}
```
Now, we have looked already what happens if there are 2 dependencies. It will throw an error as its not able to define a dependency as nothing is defined as primary.

So, we can mark one of the implementation with @Primary.

```java
@Component
@Primary
public class NormService implements Service{
}
```

**Another way to do this is auto-wiring by name.**

```java
@Component
public class ClientApp{
    @Autowired
    Service normService;
}
```

Now, it looks at NormService as the service is named with a implementing class name. But what if both types of auto wiring is present. *Then @Primary will take precedence over auto wiring by name*.

**Another way is to auto wire by @Qualifier.**

```java
@Component
public class ClientApp{
    @Autowired
    @Qualifier("spl")
    Service reqService;
}
```
```java
@Component
@Qualifier("Spl")
public class SplService implements Service{
}
```

*@Qualifier takes precedence over @Primary and auto wiring by name*


## Bean Scope

Spring creates all the beans and maintains them. By default, the scope of the bean is **singleton**.

- **singleton** - One instance per Spring Context
- **prototype** - One instance per request
- **request** - One bean per HTTP request
- **session** - One bean per HTTP session

To configure a bean as prototype, we can use the following annotation.
```java
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
```

### Singleton vs Prototype Scope

| Singleton | Prototype|
| ---| ---|
| Default Scope, no explicit annotation required| Need to set the scope explicitly like shown above |
| Only one instance per Spring Container| One instance per request|
| Same object returned each time injected| New object created each time injected|
| Used for stateless scenarios| Used for stateful|

We will look at further concepts like Life Cycle of a bean, Application context, etc in future posts
