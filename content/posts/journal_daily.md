+++ 
date = 2021-12-19T14:26:32Z
title = "Journal Daily"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Personal Development", "Journal"]
categories = ["Personal Development"]
externalLink = ""
series = ["Personal Development"]
+++

# Journal Daily

Just for a change from the usual tech articles here, I thought of giving some *personal development* content a try. One of the few habits that I have had on and off is *journaling*. I kept a journal which was more of a lifelog, where I used to log my daily encounters, learnings, and struggles. 

I have heard that journaling can help with mental health, lead to a better life, etc. But to me, journaling is more about tracking my progress. So, here are a few tips on how to get started with journaling and some common things to log

## Tips on starting Journaling

- As with any new habit, repetition is key. The best time to journal is either in the morning as soon as you wake up or before you go to bed. Tying up journaling with either waking up or going to sleep is a easy to keep track of the activity.

- Start small. Journaling is meant to be relaxing and should be done your way. So, start small by logging the weather of the day, a few lines on how the day went, what you want to achieve that day, etc.

- Invest in some good stationery. I look forward to writing with my special pen in my special notebook. It's not necessary, but it sure does help.

## Things to journal

Anything can be a journal topic, ranging from an exercise journal to a book journal. Some of the common things to journal are,

- Things happening in the day
- Thoughts and feelings
- Quotes
- Goals for the day
- Things to do
- Dreams
- Things you are grateful for
- Things you are proud of
- Travel journal
- Project journal
- Progress tracking
- Anime you watch
- Books you read

As you can see, anything can be a journal topic. 

So, for the coming New Year, why not start a journal. You will cherish it later when you flip through the pages down the road a few years later.