+++ 
draft = "true" 
date = 2024-01-08T03:00:00Z
title = "Java Concurrency 2"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Java", "Concurrency"]
categories = ["Java"]
externalLink = ""
series = ["Java"]
+++

This is part 2 of a series. Please checkout part 1 for some context.


### Thread Coordination

We will look at ways for threads to communicate with each other. We will look at how to stop threads and join them as well. We will later look at how to share data between threads.
#### Thread Termination

We can stop a thread by two ways

- Thread.interrupt()
- Daemon Threads

##### Why and when to interrupt threads?
- Threads consume resources
	-  Memory and kernel resources
	- CPU Cycles and cache memory
- If thread has completed its work but application is still running, we would need to clean up the thread's resources.
- If thread is misbehaving, we want to stop it.
- By default, app won't stop as long as at least one thread is running.

###### Thread Interrupt
We can use a thread interrupt in the following cases,
- If thread is executing a method that throws InterruptedException.
- If thread's code is handling the interrupt signal explicitly.
Let's see both with an example.

Here we have a Thread called BlockingTask, that has a InterruptedException thrown. 
```java
public static void main(String[] args) {  
    Thread thread = new Thread(new BlockingTask());  
    thread.start();  
    thread.interrupt();  
}  
public static class BlockingTask implements Runnable{  
    @Override  
    public void run() {  
        try {  
            Thread.sleep(100000);  
        }  
        catch (InterruptedException e){  
            System.out.println("Exiting blocking thread");  
        }  
    }  
}
```
The output of the above block would be,

```sh
Exiting blocking thread
```
If the interrupt is not added we can see that the main thread is waiting for the thread to finish.

![Thread state waiting](/images/posts/java_concurrency_2/waiting_threads.png)

Now, let's see what we can do for a thread where come intense computation occurs. We need to handle the interrupt, since just calling interrupt wont be enough.

```java
public static void main(String[] args) {  
    Thread thread = new Thread(new FactorialThread(new BigInteger("1000000")));  
    thread.start();  
    thread.interrupt();  
}  
public static class FactorialThread implements Runnable{  
    BigInteger no;  
    public FactorialThread(BigInteger no) {  
        this.no = no;  
    }  
    @Override  
    public void run() {  
        System.out.println("The factorial of " + no + " is " + factorial());  
    }  
    private BigInteger factorial(){  
        BigInteger ans = BigInteger.ONE;  
        for (BigInteger i = BigInteger.ONE; i.compareTo(this.no) < 1; i = i.add(BigInteger.ONE)) {  
	        //Check if thread is interrupted
            if (Thread.currentThread().isInterrupted()){  
                System.out.println("Interrupted and exiting the thread");  
                return BigInteger.ZERO;  
            }  
            ans = ans.multiply(i);  
        }  
        return ans;  
    }  
}
```

Here, we have added a condition to check if thread is interrupted to manually exit the operation.

##### Daemon Threads

They are non priority threads, such as a code Editor trying to save the file automatically. Or, threads which we have no control over such as a thread calling an external library.

```java
thread.setDaemon(true);
```
A single line is enough to make the thread a daemon thread. Now, the app won't wait for it to finish to exit the application.


### Why use threads?

We use threads for the following reasons.

- Responsiveness - Concurrency
- Performance - Parallelism