+++ 
date = 2021-12-11T14:26:32Z
title = "Jenkins Pipelines"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Devops", "Jenkins"]
categories = ["Devops"]
externalLink = ""
series = ["Devops"]
+++

# What is Jenkins Pipeline?

**Jenkins Pipeline** allows us to write the ***Jenkins build steps in code***. Build steps allow us to build(compile), test and deploy in code. Having these in code allows us to put them in *version control*.

Earlier we saw about **Jenkins Job DSL**, so what's the difference between these 2. Both allow us to write CI/CD in code, but the difference is in implementation. Job DSL creates **new jobs**, whereas Jenkins Pipeline is a **job type**.

## Using Jenkins Pipelines with Jenkins Job DSL

- Use Jenkins Job DSL to create **new pipeline jobs**.
- Use **Organization folder**, which is a feature of Jenkins pipeline to detect new project repositories automatically, removing need for new jobs.

## Example Nodejs and Docker Jenkins pipeline

First for a Jenkins Pipeline, we will write a **Jenkinsfile**. Jenkinsfile can be written in 2 ways,
- Declarative
- Scripted
 
Sample file [link](https://github.com/vigneshm243/simple-nodejs/blob/main/Jenkinsfile) and code below. We are using a scripted pipleline.

```
node {
   def commit_id
   stage('Preparation') {
     checkout scm
     sh "git rev-parse --short HEAD > .git/commit-id"                        
     commit_id = readFile('.git/commit-id').trim()
   }
   stage('Test') {
     nodejs(nodeJSInstallationName: 'nodejs') {
       sh 'npm install'
       sh 'npm test'
     }
   }
   stage('Docker build and push') {
     docker.withRegistry('https://index.docker.io/v1/', 'dockerhub') {
       def app = docker.build("vigneshm243/nodejs-test:${commit_id}", '.').push()
     }
   }
}
```

Let's first understand the code here. 

## Syntax and keywords

- **node** - Means can run on any node. It define the agent on which the pipeline should be executed.
- **def** - To define variables to be used across steps.
- **stage** - A high level sequence structure to the job. Shows up as columns in Pipeline Stage view with average stage times and colours for the stage result
- **steps** - Everything inside the stages are steps. Here, *sh* is a step.

## Stages

We have split our Jenkins Pipeline into 3 stages

- Preparation - Pulling latest code from github
- Test - Running node js installation and test
- Docker Build and Push - Self-explanatory, uses the Credential we have setup already for Docker hub.

We should ideally define a jenkins job DSL to create a job pipeline. But for demo, we will create it manually.

1. Create a new item, name it any name you want, and choose type as Pipeline.
2. The only thing to do in the configuration page is in the Pipeline section.
    1. Choose Definition as Pipeline script from SCM.
    2. Choose SCM as git
    3. Enter the git URL.
    4. Select the branch you want to build.
    5. Script path by default will be Jenkinsfile in project root directory. You can choose a custom one if required.
    ![Jenkins Pipeline](/images/posts/jenkins-job-pipeline/jenkins_pipeline.png)
3. We can give build to the pipeline and see the output.
    
We will explore further on how to configure worker nodes and have all the steps run in docker.