+++ 
date = 2021-12-06T14:11:39Z
title = "My Hugo Blog Setup"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Hugo", "Static Site Generator", "Blog"]
categories = ["Hugo"]
externalLink = ""
series = ["Hugo"]
+++

When I wanted to start a page of my own, I wanted something that is easy to maintain, easy to setup and loads quickly. This lead me to static site generators. 

Rather than going for a full **CMS(Content Management System)** with a DB, server serving up some dynamic content, etc, I wanted something that just serves my blogs. I finalized **Hugo** as my static site generator which is based on **Go**. A **Static Site Generator** basically takes *markdown* and converts them into *markup*.

This site uses [Hugo](https://gohugo.io/) and the [Coder](https://github.com/luizdepra/hugo-coder) theme. The theme is quite simplistic and serves my purpose well. For hosting, I went with [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) which is quite good for serving static content. Also, the hugo build and deploy part happens using [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/). So, when I check in a blog post, it triggers the hugo build which builds the static assets and serves it to the users who visit the website. 

Other than that, this site uses [Let's Encrypt](https://letsencrypt.org/) for SSL and [Cloudfare DNS](https://www.cloudflare.com/dns-y/) for DNS.