+++ 
date = 2021-12-20T10:56:47Z
title = "Azure Powershell and CLI"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Azure"]
categories = ["Azure", "AZ-104"]
externalLink = ""
series = ["AZ-104"]
+++

# Azure Powershell and CLI

Scripting is an important part of being an Azure admin. There are three main ways in which we can access resources in Azure. They are,

- Azure Portal - Point and click interface
- Azure PowerShell AZ - Scripting
- Bash/CLI - Scripting

## Why scripting?

The need for scripting is,

- **Automation** - To perform repetitive tasks in scripts so that they can be done automatically.
- **Source Control** - To track changes to processes that are being followed.
- **Reduce Errors** - To reduce errors that can occur when doing the steps manually.
- **A form of documentation** - A living documentation of the processes followed by the team.

## Memorizing PowerShell and CLI Commands

You get used to these commands the more you use them, but Microsoft does follow some common naming patterns that make it easy to memorize them,

 ### CLI

The pattern for CLI is as follows, 

**az** \<resource> \<verb>

**Example**

 ```
 az vm list
 az vm create
 az vm delete
 ```

If there are subcategories it's as follows,

```
az network vnet list
az network vnet create
az network vnet delete
```

### PowerShell

The pattern for PowerShell is as follows,

\<verb>-**Az**-\<resource>

**Example**

```
Get-AzVM
New-AzVM
Remove-AzVM
```

The easiest way to get started with these CLI's is in the portal. The **Cloud Shell** needs no installation and can be used directly from the browser.

