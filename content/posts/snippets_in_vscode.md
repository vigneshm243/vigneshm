+++ 
date = 2023-07-21T12:00:00Z
title = "Snippets in VS Code"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Productivity"]
categories = ["Productivity"]
externalLink = ""
series = []
+++

VS Code snippets is an easy way to create a template for recurring code blocks or front matter for certain files. We will look at a small example here for a Zettelkasten header.

Here, we have a snippet that will be triggered by "---" followed by Ctrl + Space. Intellisense will also pick it up and suggest the same as well. 

- Name of the snippet: We have named it Test
- Prefix: The trigger that would invoke the snippet. "---"
- Body: The conent to be added.
- Description: A small description of what the snippet is for.

```json
"FrontMatter": {
    "prefix": "---",
    "body": [
        "---",
        "title: $1",
        "date: \"$CURRENT_YEAR-$CURRENT_MONTH-$CURRENT_DATE\"",
        "type: ${2|Daily,Literature,Permanent,Index|}",
        "description: $3",
        "tags: [$4]",
        "---"
    ],
    "description": "FrontMatter"
    }
```

Inside the body, we have a lot of control. 
- Parameters defined by $1, $2, etc, that can be cycled through quickly using tab.
- Date or time that can be auto populated.
- Set a list of values that can be quickly selected and restricted.

A small gif showing the snippet in action.

![VS Code snippets](/images/posts/snippets_in_vscode/vscode_snippets.gif)

