+++ 
draft = "true" 
date = 2022-01-18T00:00:00Z
title = "Adding Google Analytics to Hugo"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Hugo", "Blog"]
categories = ["Hugo"]
externalLink = ""
series = ["Hugo"]
+++

One of the key things that any person writing a blog would want to know, is the impact its creating. Already, we have added comments to this blog using disqus. This allows me to know if you have any questions, feedback, etc. But most of the community doesn't comment.

So, one way to track user engagement with the app is through content tracking