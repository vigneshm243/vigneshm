+++ 
date = 2024-07-14T00:00:00Z
title = "Setup a k3s Cluster at home quickly"
description = "k3s Cluster at home"
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Devops"]
categories = ["Devops"]
externalLink = ""
series = ["Devops"]
+++

I was setting up a new Fedora Server VM over the weekend and wanted to use it as a kubernetes cluster to run my containerized workloads. I usually install docker and docker-compose and get on with my work. But I wanted to set up a k8s cluster for fun. When looking up online I found a quick and easy way to set it up. 

### Why k3s over k8s?

Rather than going with a full-blown k8s cluster with multiple VMs, I went with a single node k3s setup. k3s is a lightweight k8s, it has a smaller footprint and is great for a home lab.

I would encourage you to check out their docs for more info based on your requirements, but the gist is as follows.

### Requirements

The minimum hardware requirement is 

|Spec | Min | Recommended |
------|-----|----------------|
|CPU | 1 core | 2 core |
|RAM | 512 MB | 1 GB   |

I am running this on a Fedora Server VM with 4 cores and 8 GB of RAM, so well above the recommended settings. 

### Installation

The command to install is,

```sh
curl -sfL https://get.k3s.io | sh -
```

It will ask for the sudo password. Have a look at the script before running it at [Installation Script](https://get.k3s.io). Also, they have a lot of options to customize the components installed. Check out the full list at [Config Options](https://docs.k3s.io/installation/configuration).

### Verify the install

If everything goes well, you should be able to check the service through systemctl.

```sh
$ systemctl status k3s
● k3s.service - Lightweight Kubernetes
     Loaded: loaded (/etc/systemd/system/k3s.service; enabled; preset: disabled)
    Drop-In: /usr/lib/systemd/system/service.d
             └─10-timeout-abort.conf
     Active: active (running) since Sun 2024-07-14 18:47:30 IST; 21min ago
       Docs: https://k3s.io
```

### Check the default k3s objects

Running the following command gives you all the k3s objects deployed.

```sh
$ sudo kubectl get all -n kube-system
NAME                                         READY   STATUS    RESTARTS   AGE
pod/coredns-6799fbcd5-svqqf                  1/1     Running   0          26m
pod/local-path-provisioner-6f5d79df6-d8h5g   1/1     Running   0          26m
pod/metrics-server-54fd9b65b-5xxq4           0/1     Running   0          26m

NAME                     TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                  AGE
service/kube-dns         ClusterIP   10.43.0.10 <none>        53/UDP,53/TCP,9153/TCP   26m
service/metrics-server   ClusterIP   10.43.198.179 <none>        443/TCP                  26m

NAME                                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/coredns                  1/1     1            1           26m
deployment.apps/local-path-provisioner   1/1     1            1           26m
deployment.apps/metrics-server           0/1     1            0           26m

NAME                                               DESIRED   CURRENT   READY   AGE
replicaset.apps/coredns-6799fbcd5                  1         1         1       26m
replicaset.apps/local-path-provisioner-6f5d79df6   1         1         1       26m
replicaset.apps/metrics-server-54fd9b65b           1         1         0       26m

```

### Configuring k3s

Create and edit the following file to configure the k3s setup.

```sh
sudo vi /etc/rancher/k3s/config.yaml
```

Check out all the available config at [k3s Config](https://docs.k3s.io/installation/configuration#configuration-file).

Post, the creation/updation of the config file, restart using systemctl.

```sh
sudo systemctl restart k3s
```

### Stopping k3s

Just use systemctl to stop the service.

```sh
sudo systemctl stop k3s
```

### Unintalling k3s

Uninstalling is also a breeze with k3s, as it generates an uninstall script as soon as you install k3s at */usr/local/bin/k3s-uninstall.sh*.

Just run the script to uninstall.

```sh
/usr/local/bin/k3s-uninstall.sh
```

Post uninstall and check the status with systemctl.

```sh
systemctl status k3s
```