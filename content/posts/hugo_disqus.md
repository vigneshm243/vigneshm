+++ 
date = 2021-12-12T12:11:39Z
title = "Hugo Disqus Integration"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Hugo", "Disqus"]
categories = ["Hugo"]
externalLink = ""
series = ["Hugo"]
+++

When I wanted to add a comment section to my blog, the first platform that came to my mind was **Disqus**. It's quite easy to set up and you get a lot of features.

Some of the key features of Disqus are,

- Spam protection
- Awesome moderation features
- Easy integration with Twitter, Facebook, and Google. 
- Ease of use and setup
- A separately hosted comments section
- A single dashboard for multiple sites

The first thing required is to create a Disqus account and get your short name. Once that is got, all we need to add is the below line in your toml file.

```
disqusShortname = 'yourdiscussshortname'
```

Disqus has its internal template available, to render it, we just need to call the template.

```
{{ template "_internal/disqus.html" . }}
```

That's it, you can see the working demo under the blog posts all over this site. A Disqus account and 2 lines of code have enabled comments throughout this site.