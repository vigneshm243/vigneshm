+++ 
date = 2021-12-08T12:56:47Z
title = "Node.js"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Node.js"]
categories = ["Node.js"]
externalLink = ""
series = ["Node.js"]
+++

We will take an analytical view of Node.js in this post, why we should consider it, along with some of the advantages and disadvantages of Node.js.

## What is Node.js?

>[Node.js](https://nodejs.org/en/) is a JavaScript runtime built on Chrome's V8 JavaScript Engine.

Node.js is an asynchronous event-driven JavaScript runtime, meaning it can handle many connections concurrently. Upon each connection, the callback is fired, but if there is no work to be done, Node.js will sleep.

## Advantages

This is contrary to today's common concurrency  models, where OS threads are employed(Like Java). Thread-based networking is relatively inefficient and difficult to use. But in Node.js, users are free from worrying about deadlocks as there are no locks. 

Node.js uses something called the **event loop** as a runtime construct. Instead of having a blocking call to start the event-loop, Node.js has no start for the loop. Just after executing the input script, Node.js enters the event loop. It exits after there are no more callbacks to perform. 

HTTP is a first-class citizen in Node.js, designed with streaming and low latency in mind. This makes Node.js well suited for the foundation of a web library or framework.

Although Node.js has no threads, we can still take advantage of multiple cores. It provides APIs via which child processes can be spawned and are easy to communicate with. This allows sharing sockets between processes to enable load balancing over cores.

## Disadvantages

Though all these advantages are there, we do have a few places where Node.js doesn't shine.

Node.js can't handle computation heavy tasks as well as it can handle I/O intensive operations. Node.js and its third-party modules change very rapidly, which can lead to some stability issues. Its asynchronous coding style is difficult to maintain.

We will look further at the Event Loop which is an interesting construct to explore in a future post.