+++ 
date = 2023-07-21T12:00:00Z
title = "Git 1"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Git"]
categories = ["Git"]
externalLink = ""
series = ["Git"]
+++

## Git

Git is a source control program. It can be used to,

- [[Version control]] (maintain all version of a project)
- Revert to older version if something breaks
- Colloborate with people (allowing multiple people work on a single project)
- Track all the changes done by multiple people

In this article, we just explore all the features we can use locally. 

## Using Git

### Create a git repo

The way to create a git repo is to run the git init command in the project folder

```bash
git init
```

Git will create a folder called .git in the current working directory. 

### Start tracking

From now on, all the changes done in the folder will be tracked. Try adding a file and just check it out.

```bash
git status
```

This will show unstaged files i.e files not yet commited in red, committed files in green.

### Commiting the changes

If we commit the changes, it will create a snapshot of the version. But before that the changes need to be added to staging.

```bash
git add .
```

To add the files to staging using the above command. Now git status will return the files in green. Now the changes can be commited.

```bash
git commit -m "Changes done"
```

This will now create a snapshot with the commit.

### Tracking changes

To checkout the history of the changes, we can use git log.

```bash
git log
```

### Reverting a commit

To revert to an older version, we choose the version needed from git log and use git reset to go back to previous commit. This will revert the changes and unstage the changes done as well.

```bash
git reset $prev_commit_hash
```

A git status should show us the changes done after the reverted hash in red.

If we use the flay --hard, it removes the files as well from the working directory, so use with caution if required.

```bash
git reset --hard $prev_commit_hash
```

### Stash changes

Suppose we need to temporarily move the changes and get it to previous committed version, we can use git stash.

```bash
git stash
```

To bring back from stash, we can bring it back using git pop

```bash
git stash pop
```

We can stash and remove the changes as well.

```bash
git stash clear 
```

To remove just the top of stash changes we can use git stash drop

```bash
git stash drop
```

### Getting the changes back after a git reset --hard

There is another set of logs in git called reference logs. The command to access reference logs is reflog

```bash
git reflog
```

We can use this to checkout a branch from here to an older before a hard reset was done to get the changes.

In part 2, we will explore the feature we can use to colloborate.