+++ 
date = 2022-03-29T12:11:39Z
title = "Linux Commands to boost productivity "
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Terminal", "Linux"]
categories = ["Linux"]
externalLink = ""
series = ["Linux"]
+++

# Linux Commands to boost productivity 

I have been using the terminal for quite a long time. And few of these commands which I recently came across have changed the way I use the terminal. Hope you guys will find these useful as well.

Let's get started with the commands,

## Quickly navigating directories

One quick tip is to navigate to home, we can use
```bash
->cd ~
```
Similarly, we can use - to navigate back to the previous working directory
```bash
->cd -
```

## Clear screen

We usually type clear to clear the screen, alternatively just press **Ctrl + L** to move the prompt to the first line. This just moves the prompt to the top of the screen. This is quite quick and I started using this quite frequently compared to clear.

## Minimize a program

Okay, technically we are not minimizing in the sense. But we can press **Ctrl + Z** to move the code to pause the program. So, we can be editing in vim and press Ctrl + Z and pause it, do something else, then get back to editing by using the **fg** command.

## Repeating the last command with sudo

We missed adding sudo to a command. This happens so often that it's frustrating to hit home and type sudo or retype the entire command after sudo. One simple way to get over it is to use the following command.

```bash
->sudo !!
```

!! is referred to as bang bang. So this command is referred to as *sudo bang bang*.

## Review command history

We know about history command. But press **Ctrl + R** to open up a search. This is far more convenient and easier to search. Press Ctrl R to cycle through the commands.

## Run command from history

Execute the history command, note the number before the command you want to run. Type bang followed by the number to execute the command.

```bash
->history
1 apt get update 
2 apt get upgrade
->!2
```

## Clear line

Press **Ctrl + U** to clear the entire line.

## Go to start and end of line

Press **Ctrl + A** to go to start of the line
Press **Ctrl + E** to go to end of the line

## Chaining commands

We can use ; or && chain commands.

```bash
->ls -a test; echo hello
ls: cannot access 'test': No such file or directory
hello
->ls -a test && echo hello
ls: cannot access 'test': No such file or directory
```

As you can see the key difference is that; doesn't care about the output of the previous command. Whereas, when we chain with & it needs the previous command to complete successfully for the second command to execute.

## Column command

This is just to prettify columnar outputs. Any jumbled output can be made easier to read with a column command. This can be used to format output as required as well.

```bash
mount| column -t
```

## Increase and decrease font size

This is one thing I have seen new developers struggle with. A quite intuitive command set to increase and decrease font sizes are 

**Ctrl + +** to increase font size
**Ctrl + -** to decrease font size

I will be on the look out for more commands that can help me in my daily work flow. Please feel free to let me know in the comments below about any more commands that might be useful.