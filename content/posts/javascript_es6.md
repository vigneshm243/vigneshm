+++ 
date = 2022-04-29T12:56:47Z
title = "JavaScript Refresher"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["JavaScript"]
categories = ["JavaScript"]
externalLink = ""
series = ["JavaScript"]
+++

# JavaScript Refresher

JavaScript ES6 came out in 2015 and it's been quite a long time after that. I am just gonna walk through some of the core features that got introduced in ES6 that are important to understand.

## let and const

**var** used to be the de facto variable declaration keyword used in JavaScript till ES6. Now, it's encouraged to use **let and const**. First, let's go through what **var** does. 

**var** if declared outside functions, is **globally scoped**. It's hoisted to the top and initialized to undefined. We can redeclare the variable again as well. Some problem with this is that we might be using the variable before initialization and expect an error, but we will get undefined. Also, variables using var can be re-declared and used for another purpose. This is especially true in big projects.

Below, we might expect an error or forget to initialize the variable. But we get undefined printed.

```js
console.log(test);//undefined
var test = "Hi";
```

Below, we might have unknowingly used the same variable for another purpose.

```js
var test = "Hi"
var no = 5
if (no == 5) {
    var test = 0;
}
console.log(test);//0
```
We also might have scenarios where we want the variable to be not changeable(constants). All these are solved by let and const. The below table will give you an idea of how it does it.

|                                | **var**            | **let**         | **const**       |
|--------------------------------|--------------------|-----------------|-----------------|
| Scope                          | Global or Function |      Block      | Block           |
| Re declaration                 | Yes                | No              | No              |
| Hoisting                       | Yes                | Yes             | Yes             |
| Initialized to                 | Undefined         | Not initialized | Not initialized |
| Declare without initialization | Yes                | Yes             | No              |

## Arrow functions

Arrow functions are the norm nowadays in JavaScript. A normal JS function looks like the below snippet.

```js
function myDobuleFun(no){
    return no * 2;
}
```

As an arrow function, we can write it in a couple of ways. Let's have a look at those.

```js
const myDoubleFun = (no) => {
    return no * 2;
}
```

The below syntax is only applicable when we have a single return statement and a single argument. But for those cases, this syntax does make the code a lot cleaner.

```js
const myDobuleFun = no => no * 2;
```

## Exports and Imports(Modules)

We can write modular code with ES6. The way to do it is through exports and imports. Let's look at export and import as a few examples.

The below is a default export. We will look at how this can be imported.
```js
//test.js
const test = 10;
export default-test;
```
This is a default import, we can name it whatever we want as there is only one export that is marked default.
```js
//app.js
import test from './test.js'
import t from './test.js'
```

Below, we are using named export and import. As there are multiple ones, we are using named imports.
```js
//util.js
export const pi = 3.14;
export const e = 2.718;
```
Below, we are using named imports, we can import one by one using the name or as a bundle. We have named the bundle as bundled and the imported properties can be accessed via bundled.pi or bundled.e.
```js
//app.js
import {pi} from './util.js'
import {e} from './util.js'
import {pi as math_pi} from './util.js'
import * as bundled from './util.js'
```

## Spread and Rest Operators

Both are represented by **...**.

**Spread** is used to **split up array elements or object properties**.

```js
let no = [1, 2, 3];
let newNo = [...no, 4];//[1, 2, 3, 4]
let newNo = [no, 4];//[[1, 2, 3], 4]
```

**Rest** is used to **merge a list of function arguments into an array**.

```js
const filter = (...args) => {
    return args.filter(e1 => e1 === 1);
}
```

## Destructuring

This is used to extract array elements or object properties and store them in variables.

```js
[a, b] = ['Hi', 'World'];
console.log(a); //Hi
console.log(b); //World
{name} = {name: 'John', age: 20};
console.log(name)//John
console.log(age)//undefined
```

So, hope these might add value to your day-to-day workflow. We will dive into ReactJS in a future post to dabble around some of these in action.