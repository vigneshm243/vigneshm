+++ 
date = 2023-01-21T12:00:00Z
title = "Reliable, Scalable and Maintainable Applications 2"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["DDIA", "System Design"]
categories = ["System Design"]
externalLink = ""
series = ["DDIA"]
draft = "true"
+++

This is a part 2 of an article. Please visit [here](https://vignesh.page/posts/reliable_scalable_maintainable1/) for part 1. 

We saw in detail about Reliability in the last article. Here it would be a continution towards Scalability and Maintainability.

## Scalability

Even a system that is reliable today, may not be reliable tomorrow under inscreased load.

The ability of an system to cope with inscreased load is termed Scalability. Since, Scalability is defined in terms of load it's important to describe load.

### Load

Load can be defined by few attributes called *load parameters*. Parameters depends on the architecture, it can be 
- requests per second to a web server 
- the ratio of reads to writes in a database
- the number of simultaneously active users in a chat room
- the hit rate on a cache

## Describing Performance

Once load is described, we can check what happens 

Latency and response time are often used synonymously, but they are not the same. The response time is what the client sees: besides the actual time to process the request (the service time), it includes network delays and queueing delays. Latency is the duration that a request is waiting to be handled—during which it is latent, awaiting service