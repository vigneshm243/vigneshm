+++ 
date = 2021-12-04T12:49:11Z
title = "First Jenkins Job for Nodejs Docker deployment"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Devops", "Jenkins", "Docker"]
categories = ["Devops"]
externalLink = ""
series = ["Devops"]
+++

In this post, we will create our first job to do the following tasks automatically,
1. Pull the latest code from a git repository.
2. Create a docker image using the Dockerfile in the repository.
    1. This will take care of npm install and exposing of the ports.
3. Publish to a docker hub repository.

## Prerequisite Nodejs plugin and Docker plugin

A prerequisite for building node js applications is to have the nodejs plugin installed in Jenkins. We have a lot of plugins in jenkins that makes it easy to integrate with third party applications, perform frequently done operations easily, etc. 

- To add a plugin, we have to navigate to **DashBoard > Manage Jenkins > Manage Plugins**.
![Manage Jenkins add plugins](/images/posts/jenkins_nodejs_app/jenkins-add-plugin.png)
- Navigate to available plugins, search for **NodeJS** plugin and choose Download now and install after restart. Select restart when no jobs are running to initiate restart when no jobs are running.
![Manage Jenkins add nodejs plugins](/images/posts/jenkins_nodejs_app/jenkins-add-nodejs-plugin.png)
- Similarly, download and install the Docker plugin as well. We will be using **CloudBees Docker Build and Publish** plugin for this peoject.

## Setting up the job

- To create a new job, click on new item, give a name for the job and choose **Freestyle project**. You will land at the configuration page.
- We will configure the **Source Code Management** as <em>git</em>, give the git repository url and specify the branch as master.
![Git link config](/images/posts/jenkins_nodejs_app/jenkins-nodejs-git.png)
- We will then add the build step. We will choose **Docker Build and Publish**. We will fill in the docker repository name and supply the registry credentials. Here we have used a docker hub repository(https://hub.docker.com/repository/docker/vigneshm243/nodejs-test).
![Build config](/images/posts/jenkins_nodejs_app/jenkins-nodejs-build.png)
- We can check the results by issuing build now and check the console output. From the console output, we can see the steps getting executed sequentially.
- Finally, we can check the image updated in **Docker Hub**.
![Docker hub](/images/posts/jenkins_nodejs_app/jenkins-docker-hub.png)

We will look at more complex jobs in further posts and explore few alternativies like gitlab-ci and github actions.