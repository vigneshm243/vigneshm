+++ 
date = 2021-12-03T09:47:37Z
title = "Jenkins Installation"
description = "Jenkins Installation"
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Devops", "Jenkins"]
categories = ["Devops"]
externalLink = ""
series = ["Devops"]
+++

We will take a look at how to install Jenkins. Jenkins is typically run as a standalone application in its own process with the built-in Java servlet container/application server (Jetty).

We will look at a one of the easiest ways to install Jenkins using docker.

### Docker

One of the easiest ways is to just run the below docker command. We will get the latest offical jenkins image pulled and created. We map the home path as a persistent volume to retain our configurations.

   docker run -p 8080:8080 -p 50000:50000 -v ${pwd}:/var/jenkins_home jenkins/jenkins

You will see get a console output printing the auto generated password.

Alternatively, you can get the password from /var/jenkins_home/secrets/initialAdminPassword folder. Since we have mapped to our current directory, password can be found there as well.

![Jenkins Install](/images/posts/jenkins_installation/jenkins-install-password.png)

Next, we can access the Jenkins application via http://localhost:8080. (Since I am running off a Virtual Server, I am accessing through the server IP below)

![Jenkins URL](/images/posts/jenkins_installation/jenkins-install-initial.png)

That's it, we have a fresh jenkins instance up and running for our configuration. Further, we can proceed to install required plugins and create our first admin user.