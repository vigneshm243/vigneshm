+++ 
date = 2021-12-02T18:20:15Z
title = "What is Jenkins?"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Devops", "Jenkins"]
categories = ["Devops"]
externalLink = ""
series = ["Devops"]
+++
# What is Jenkins?

Jenkins is a open source CI/CD tool written in Java. Its an automation software to build and deliver software projects. A major benifit is it has a lot of plugins, open source, free and popular.

## What is CI/CD?

**Continous Integration(CI)** is a practice of merging multiple developer working copies to a shared machine several times a day.

**Continous Delivery(CD)** is a software engineering approach by which teams can deliver software in short cycles, ensuring that they can be reliabily released any time

## In practice

**Verify and Publish** work by triggering automated builds and tests.

- For **every commit** or atleast once a day.
- All developers push the code to a version control, which should then be built and tested - which will be done with Jenkins.
- Jenkins doesn't merge code or resolve any conflicts, these are still left to the developers.

## Benefits

- Provides a **quick feedback loop** for the developer to fix build errors.
- Jenkins can **publish** every build of the application

## CI/CD within SDLC

![CI/CD within SDLC](/images/posts/what_is_jenkins/CI_CD_in_SDLC.png)

## Jenkins alternatives

- GitHub Actions
- GitLab
- AWS CodePipeLine
- JetBrains TeamCity
- Spinnaker
