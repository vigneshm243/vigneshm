+++ 
date = 2021-12-14T10:56:47Z
title = "Azure Cloud Basics"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Azure"]
categories = ["Azure", "AZ-104"]
externalLink = ""
series = ["AZ-104"]
+++

# Azure Cloud Basics

We will look at few key cloud terminologies that you would need to know.

## Virtual Machines

- Windows or Linux OS
- Can be remotely accessed via RDP or SSH
- Can be created quickly, less than 10 mins
- Can install anything on it
- Placed in a virtual network

Virtual machines are one of the foundational building blocks of cloud computing. Some virtual machine abstractions; i.e. services built on top of virtual machines; are,

- Azure Batch
- Virtual Machine Sets
- Azure Kubernetes Service(AKS)
- Service Fabric

## App Service

- Web Apps or container apps
- Fully managed servers, no remote access
- Lots of benefits in scaling, CI, deployment slots, integration with VS Code
- Java, Node.js, PHP, Python, etc

## Storage

- Create storage accounts upto 5 PB
- Blobs, Queues, tables, files
- Various levels of replication - global, local
- Different storage tiers - hot, co0l, archive

## Data Services

### SQL Related

- Azure SQL DB
- Azure SQL Managed Instance
- SQL Server on VM
- Synapse Analytics(SQL Data Warehouse)


### Other

- Cosmos DB
- Azure DB for MySQL, MariaDB, PostgresSQL
- Azure Cache for Redis

## Microservices

- Service Fabric
- Azure Functions
- Azure Logic Apps
- AKS(Azure Kubernetes Services)