+++ 
date = 2021-12-16T07:56:47Z
title = "What is Spring?"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Java", "Spring"]
categories = ["Spring"]
externalLink = ""
series = ["Spring"]
+++

# What is Spring?

Spring is a **Dependency Injection Framework**.

## What is Dependency Injection?

Dependency Injection is a technique in which an object receives its dependencies. The receiving object is called a *client*., and the passed-in(injected) object is called a service. The code that does this operation is called an injector. So, instead of the client telling which service to the user, the injector will tell which service to use. Injection refers to passing dependency into the client.

The service is made part of the client's state. Passing the service to the client, rather than allowing the client to build or find the service, is the fundamental requirement of the pattern.

The intent behind dependency injection is to achieve separation of concerns of construction and use of objects. This can increase readability and code reuse.

Dependency injection is a form of inversion of control. The client should not know how to construct a service object. Instead, the client delegates to external code (the injector). The client is not aware of the injector. The injector passes the services, which might exist or be constructed by the injector itself, to the client. The client then uses the services.

This means the client does not need to know about the injector, how to construct the services, or even which services it is using. The client only needs to know the interfaces of the services, because these define how the client may use the services. This separates the responsibility of 'use' from the responsibility of 'construction'.

This enables loose coupling. Let's see some examples,

## Tight Coupling

Below is an example of tight coupling, where the Client is instantiating the Service it needs. Suppose we want to change the Service used, we would need to rewrite the client and recompile.

```java
public class ClientApp{
    Service reqService =  new SplService();
}
```
```java
public class SplService implements Service{
```

## Removing Tight Coupling

We can remove tight coupling by using a **constructor or setter** to avoid Service creation in the client app.

```java
public class ClientApp{
    Service reqService;
    public ClientApp(Service reqService){
        this.reqService = reqService;
    }
}
```
```java
public class SplService implements Service{
```

In the below usage, we create a new instance of the app after creating the dependencies. This below functionality to creating objects and populating dependencies.

```java
SplService splService = new SplService();
ClientApp app = new ClientApp(splService);
```

So, Spring can be used to do this. *Spring will take care of instantiating the dependencies and populating the dependencies.*

We will see a small snippet below using Spring dependencies.

```java
@Component
public class ClientApp{
    @Autowired
    Service reqService;
}
```
```java
@Component
public class SplService implements Service{
}
```

We have used 2 annotations here,

@Component - Spring should manage the instance of the class.
@Autowired - Spring will look among the components and find the dependency and inject.

## Few Key Terminologies

- **Bean** - Instances managed by Spring Framework
- **Autowiring** - Process of Spring identifying the dependencies and identifying the matches for the dependencies and population of it.
- **Dependency Injection** - Injection of dependency into a client.
- **Inversion of Control** - Taking control from the *client* and giving it to the framework
- **IOC Container** - Anything that implements the IOC. Application Context in Spring. Create, manage the bean instances
- **Application Context** - Application Context is the component that takes care of IOC implementation