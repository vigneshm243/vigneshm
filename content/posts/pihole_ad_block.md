+++ 
date = 2021-12-15T10:56:47Z
title = "Pi-hole Ad Block"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Pi-hole", "Docker"]
categories = ["Pi-hole"]
externalLink = ""
series = ["Pi-hole"]
+++

# Pi-hole in Docker

If you want to block ads and stop trackers, one good tool to use is [**Pi-hole**](https://pi-hole.net/). Pi-hole enables network wide ad block, not just a single device. 

## How it works?

Pi-hole acts as a DNS server. So, every request made in your network looks up in the DNS to resolve the IP. Pi-hole checks if the request is going to a blocked IP based on the blacklisted IPs. So, when a page tries to fetch the ad, it gets no response and is unable to load the ad. 

## Installing Pi-hole on Docker

One of the quickest and easiest way to setup Pi-hole is to run it on a Docker container. I would suggest you to run it on a Raspberry Pi as intended as its a low cost always on option. 

- First, we will write a docker-compose.yml, you can find it in this [link](https://github.com/vigneshm243/pi-hole-docker/blob/main/docker-compose.yml).

- We will run the compose file.

```
docker-compose up -d
```

- Next, we need to change the default password, execute the below commands to move into the container and change password. Once done exit the container.

```
docker exec -it pihole bash
pihole -a -p
```

- Access the URL. Naviate to http://localhost/admin/.

![Pi-hole](/images/posts/pi-hole/pi-hole.png)

- That's it, you can explore the admin panel, add more sites to block as per your requirement