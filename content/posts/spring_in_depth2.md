+++ 
date = 2021-12-21T08:56:47Z
title = "Spring in depth 2"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Java", "Spring"]
categories = ["Spring"]
externalLink = ""
series = ["Spring"]
+++

# Spring in Depth 2

## Component Scan

By default, when we annotate a class with @SpringBootApplication, it performs a component scan because it's a combination of @ComponentScan, @EnableAutoConfiguration and @Configuration.

@ComponentScan by default scans the package where the class is and the sub-packages of the package where the class is present. So what if you want to access beans not present in the same package. 

By default, @ComponentScan means @ComponentScan("\<package its present>"). So, if a specific package needs to be scanned, then we need to specify the package.

```java
@ComponentScan(basePackages = "\<package to scan>")
```

If the bean is not present in the same package or @ComponentScan is missed, then it results in bean not found error.

Like how we are including packages, we can exclude them as well,

```java
@ComponentScan(excludeFilters = 
  @ComponentScan.Filter(type=FilterType.REGEX,
    pattern="me\\.vignesh\\.springapp\\.exclude\\..*"))
```

One thing to note would be to not put the @Configuration in the default package, which would lead Spring to scan all the packages present in classpath and fail to start up.

## Life Cycle of a Bean

The major life cycle stages of a bean in Spring IOC Container are as follows,

### 1. Bean Creation and Instantiate
Bean created and loaded in application context.
### 2. Populating Bean properties
Spring container will create a bean id, scope, default values based on the bean definition.
### 3. Post-initialization
Spring provides Aware interfaces to access application bean details and callback methods to hook into the bean life cycle to execute custom logic.
### 4. Ready to Serve
Bean is created and injected all the dependencies, all the Aware and callback methods implementation should be done. Bean is ready to serve.
### 5. Pre-destroy
Spring provides callback methods to execute custom logic and clean-ups before destroying a bean from ApplicationContext.
### 6. Bean Destroyed
Bean will be removed or destroyed from memory.

We can interface with lifecycle methods in 2 ways, 

### Spring Aware Interface

Just a list of the Spring Aware interfaces available, generally not used much in practice.

| Interface | Method| 
| ---| ---|
|BeanNameAware|public void setBeanName(String name)|
|BeanClassLoaderAware|public void setBeanClassLoader(ClassLoader beanClassLoader)|
|BeanFactoryAware|public void setBeanFactory(BeanFactory beanFactory)|
|ApplicationContextAware|public void setApplicationContext(ApplicationContext     applicationContext)|

### @PostConstruct and @PreDestroy annotations

To do something just after the bean is created, we can use the @PostConstruct.

```java
@PostConstruct
public void preDestroy() {
  //Initialize something if required
}
```

To do something, just before a bean is deleted.

```java
@PreDestroy
public void preDestroy() {
  //Clean up code, like closing a connection maybe
}
```