+++ 
date = 2022-06-28T12:00:47Z
title = "Postman for API Automation and Testing"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Postman", "Testing", "Automation"]
categories = ["Postman"]
externalLink = ""
series = ["Automation"]
+++

As I was exploring a way to automate testing of my APIs and chaining the response of previous request to next request and so on, I stumbled up on a way to create a simple Automation testing framework using Postman.

We will be able to do the following operations, 

* Run a sequence of APIs
* Pass dynamic inputs
* Choose to run specific APIs 
* Validations of the test data before request
* Assert responses

## Inside Postman

These are the steps we follow inside Postman.

1. Create a postman requests in a collection.
2. Run the same using postman runner.
3. Inputs can be dynamically passed for different test cases using data csv files.
4. Response of previous requests can be forwarded to next request using variables.
5. Environments and secrets can be parameterized using variables.

## Running via CLI and generating comprehensive reports using newman

newman is a node module that gives us a CLI for executing postman collections.

### Installation

```sh
npm install -g newman
npm install -g newman-reporter-htmlextra
```

### Usage

```sh
newman run -d data.csv --delay-request 5000 "Test.postman_collection.json"  --reporters=cli,htmlextra 
```

Reports will be generated in a folder called newman inside current working directory.

## Using nodejs to have more control and enhancing the scripts

The steps done in command line can be done via a nodejs script as well. This will give us more control than the CLI

```js
const newman = require('newman');
const path = require('path');

var testPlan = "TestPlan"; // test plan name
newman.run({
    collection: path.join(__dirname, './plan/' + testPlan + '.json'),
    // environment: {}
    iterationData: path.join(__dirname, './plan/' + testPlan + '_data.csv'),
    // iterationCount: 5, 
    // reporters: ['cli', 'html']
    folder: ["Token Fetch", "Get Details"],
    reporters: ['cli','htmlextra'],
    reporter: {
      htmlextra: {
        export: './result/' + testPlan + '.html'
      }
    }
  });
```

Here, the folder option gives us the option to select the requests to run. 

## Further to explore

* Programtically form requests and response
* Maintain the data in a DB for future if required
* Integrate with current CI to automatically run the tests and generate reports

If you like to explore more please refer the below links. 

## Reference

* https://www.freecodecamp.org/news/how-to-automate-rest-api-end-to-end-tests/	
* https://www.alphabold.com/postman-reporting-templates-using-newman/

Happy Coding!