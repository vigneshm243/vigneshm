+++ 
date = 2021-12-07T12:56:47Z
title = "Solid Principles"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Design Pattern", "SOLID"]
categories = ["Design Patterns"]
externalLink = ""
series = ["Design Patterns"]
+++

SOLID is an acronym for a set of 5 design patterns in object-oriented design(OOD). It was written by Robert C. Martin in 2000 in the essay [Design Principles and Design Patterns](https://web.archive.org/web/20150906155800/http:/www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf). I would suggest a read-through of the essay once. The term was later coined by Micheal Feathers.

SOLID expands into:

- S - Single-responsibility Principle
- O - Open-closed Principle
- L - Liskov Substitution Principle
- I - Interface Segregation Principle
- D - Dependency Inversion Principle

All these are patterns that are used daily by software engineers around the world for the advantages they provide. You might have come across them in your daily work but might not know these terms. Without proper design of software, we would end up with a lot of issues. Martin calls this **Rotting Design**. Software designed by designers always starts clean, elegant, and compelling. But it slowly starts to degrade as more and more changes are to be done to the program to meet newer requirements.  

## Symptoms of Rotting Design

There are four main symptoms of rotting design. They are interlinked with each other. They are 

- ### Rigidity: 
    It's the difficulty of software to change. When a developer starts a change in one module, it leads to changes in the dependent modules, going on in an endless thread through the application. This causes managers to become averse to change as they are not sure of the time that a change will take. So what started as a design inefficiency, becomes an adverse management policy. 
- ### Fragility:
    Closely related to rigidity, software tends to break when a small change happens. Something might break in an unrelated module for change in another module. These fixes will add to more issues rather than fixes. Credibility and trust in the software are lost.
- ### Immobility:
    Immobility here is in terms of reusability. To reuse a module, there might be too much work involved to get it free from the list of unrelated dependencies, making it easier to just rather implement a new module altogether.
- ### Viscosity:
    Viscosity comes in two forms, 
    - The viscosity of design - It's easier to change something by not following the design. These changes are some form of hack, to directly change something rather than following the design. This will eventually lead to issues down the road.
    - The viscosity of the environment - If it takes longer compile times, developers will try to avoid changes that involve recompiling the code. If it takes longer to check in the code and get it deployed, changes would be tried in such a way it takes the least check-ins.

## What causes the design to rot?

Now that we have gone through the symptoms, what causes design rot? Well, it's usually due to one of the following 2 reasons,

- ### Changing Requirement
    Changes that come in might be developed by people who were not aware of the initial design philosophy. But that does not mean the blame falls on the people changing the software, as software requirements change drastically. Designs should be resilient to these changes.
- ### Dependency Management
    Most of the above issues happen due to improper dependency management in one form or another.

So, how will we fix this? We can do that by implementing dependency firewalls between modules. Dependencies don't propagate across these firewalls. Object-oriented design is full of patterns to solve this dependency management of modules. 

We will look at each of the principles in detail in further posts.