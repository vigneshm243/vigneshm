+++ 
date = 2022-06-23T10:56:47Z
title = "Types in Go"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Go"]
categories = ["Go"]
externalLink = ""
series = ["Go"]
+++

In the last article, we saw some of the very basic constructs of Go. Here, I am gonna go over basic data types and a few aggregated data structures available in Go.

## Basic Types in Go

Below are some of the basic types in Go. Go has *Zero values*, which means it will assign a default value if nothing is initialized to the variables getting declared. Below is a list of some of the types.

| Type      | Values                                                | Zero Value | Sample value |
|-----------|-------------------------------------------------------|------------|--------------|
| bool      | Before first iteration                                | false      | true         |
| string    | Before every iteration                                | ""         | "test"       |
| int       | 32 or 64 bits integers                                | 0          | 25           |
| uint      | 32 or 64 bits unsigned integers                       | 0          | 5           |
| float32   | IEEE-754 32-bit floating-point numbers                | 0          | 1.3242       |
| complex64 | Complex numbers with float32 real and imaginary parts | (0+0i)         | (2+3i)       |

I have tried declaring and using all the different types to check the zero values and tried initializing them as well.

```go
package main

import "fmt"

func main() {
    var bZeroValue bool
    var b bool = true
    var sZeroValue string
    var s string = "test" 
    var iZeroValue int
    var i int = -5
    var uZeroValue uint
    var u uint = 5
    var fZeroValue float32
    var f float32 = 3.14
    var cZeroValue complex64
    var c complex64 = (3 + 4i) 
    fmt.Printf("Type: %T Value: %v\n", bZeroValue, bZeroValue)
    fmt.Printf("Type: %T Value: %v\n", b, b)
    fmt.Printf("Type: %T Value: %v\n", sZeroValue, sZeroValue)
    fmt.Printf("Type: %T Value: %v\n", s, s)
    fmt.Printf("Type: %T Value: %v\n", iZeroValue, iZeroValue)
    fmt.Printf("Type: %T Value: %v\n", i, i)
    fmt.Printf("Type: %T Value: %v\n", uZeroValue, uZeroValue)
    fmt.Printf("Type: %T Value: %v\n", u, u)
    fmt.Printf("Type: %T Value: %v\n", fZeroValue, fZeroValue)
    fmt.Printf("Type: %T Value: %v\n", f, f)
    fmt.Printf("Type: %T Value: %v\n", cZeroValue, cZeroValue)
    fmt.Printf("Type: %T Value: %v\n", c, c)
}
```

The output shows us the *Zero Values* and how each value is initialized and used.

```bash
Type: bool Value: false
Type: bool Value: true
Type: string Value: 
Type: string Value: test
Type: int Value: 0
Type: int Value: -5
Type: uint Value: 0
Type: uint Value: 5
Type: float32 Value: 0
Type: float32 Value: 3.14
Type: complex64 Value: (0+0i)
Type: complex64 Value: (3+4i)
```

## Go Pointers

Go also has pointers. It holds a memory address the same as C or C++. The type *T is the pointer of type T. Zero value of the pointer is nil. We use the **& operator** to generate a pointer.

```go
i := 5
var p *int = &i
```

We can access value from the pointer using *. This is called **dereferencing**.

```go
fmt.Println(*p)
```

## Go Structs

Structs are also similar to C, they are used to form a collection of typed fields. Typically we can use it to model and maintain the state of an object if we are thinking in OOPS terms.

Below is a short example

```go
type Anime struct {
    name string
    year int
}

func main(){
    onePiece := Anime("One Piece", 1999)
    fmt.Println(onePiece.name, "was first aired on", onePiece.year)
}
```

We access the struct fields using the *dot* operator.

## Go Arrays

Arrays in Go is a collection of the same type of values. The type of values, as well as the length, can't be changed after declaration.

```go
nos := [3]int {1, 2, 3}
fmt.Println(nos)
```

But, unlike C, and C++ where arrays are pointers, in Go it is the values. The implication of this difference is 2 fold,
* It copies the values when we assign the array to a new variable
* When passing to a function, it passes a copy, not a pointer.

## Go Slice

Slice looks and does the same things, but is far more flexible. It can be extended using the built-in append function. Another key difference is that the slice uses a reference. Also, all Go functions use slices rather than arrays.

A simple snippet using slice and extending it with append.
```go
no := []int{1, 2, 3, 4}
no = append(no, 5)
fmt.Println(no)
```

I hope this would help you understand some of the basic types in Go. I hope to make a detailed article on Slice and its internal working in a future article with some examples. In the next article, I will tackle Go routines with the help of a small project. 

Happy Coding!
