+++ 
date = 2022-02-28T14:26:32Z
title = "Maslow's Hierarchy of Needs"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Personal Development", "Theory of Motivation"]
categories = ["Personal Development"]
externalLink = ""
series = ["Personal Development"]
+++

Something that everyone has trouble with is finding motivation. It's the same for me. Let's explore one of the fundamental works in the study of motivation by [Abraham Maslow](https://en.wikipedia.org/wiki/Abraham_Maslow). 

According to Wikipedia,
>Abraham Harold Maslow was an American psychologist who was best known for creating Maslow's hierarchy of needs, a theory of psychological health predicated on fulfilling innate human needs in priority, culminating in self-actualization.

The theory suggests that one can't move up the hierarchy to fulfill higher needs before fulfilling lower ones. This makes sense when we look at it.  All people have the innate desire to be self-actualized, i.e to be all that we can be. To do that we need to first achieve our basic needs then move towards the higher needs.

The hierarchy goes as follows,

- Physiological 
- Safety 
- Social
- Esteem
- Self Actualization

The above can be further classified into 2; namely, deficiency needs and being needs(growth needs). 

Deficiency needs arise due to deprivation when something is denied or not obtained. For instance, the longer we go without food, the more hungry we become. The motivation here goes down as needs are met. We no longer felt hungry once we had our food. 

But growth needs don't arise due to lack of something, but out of the desire to grow. To be the best in something, to perform in front of a huge audience, to start a successful business venture, the list goes on. These needs also tend to grow stronger once we engage with them.

Deficiency needs are Physiological, Safety, Social, and Esteem needs, whereas the Growth(Being) needs are Self Actualization.

## Physiological Needs 

These are things that people need for their survival as human beings, like air, water, food, sleep, shelter, etc. Until these are met satisfactorily, all other needs will be secondary.

## Safety Needs

Once the physiological needs are satisfied, safety needs come into the picture. These can range from financial security(job, savings), social stability, property, health, etc.

## Social Needs

Every human needs to feel accepted and crave a sense of belonging. These can range from family, friendships, colleagues, social groups, religious groups. Man is after all a social animal.

## Esteem Needs

This is the need for appreciation and respect. This is to feel the sense of accomplishments, prestige, etc. Feeling valued by others and achievements in respective fields will help with this.

## Self Actualization Needs

Now, this is a little tricky to explain. Once all these things are achieved, people would want to realize their potential, have a feeling of self-fulfillment. At this stage, people would want to achieve all that they can and go for peak experiences. This is something that differs between each individual.