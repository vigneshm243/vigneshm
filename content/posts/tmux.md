+++ 
date = 2024-07-17T00:00:00Z
title = "Tmux 101"
description = ""
slug = ""
authors = ["Vignesh Muthukumaran"]
tags = ["Terminal", "Linux"]
categories = ["Linux"]
externalLink = ""
series = ["Linux"]
+++

tmux is a terminal multiplexer. We can have multiple terminals inside a single terminal, which is especially useful when we ssh into a remote machine.

![tmux cool](/images/posts/tmux/tmux_cool.png)

tmux has 3 levels of hierarchy,

- Sessions - To have completely different work environments for different concerns
- Windows - To have multiple tabs for what that window represents, basically one for monitoring logs and performance and another to execute the commands required.
- Panes - To have different panes inside a window, like one running top, and another running a couple of tail commands on log files.

To start using tmux, we just need to run the command *tmux*.

One key concept in tmux is the concept of prefix key. After pressing the prefix key, tmux enters command mode, similar to vim's insert, command, and view modes. The **prefix key** for tmux by default is **Ctrl + B**.

### Basic commands

- Start a new session
```sh
tmux
```
- Detach from a session
```shortcut
Ctrl+B d
```
- Attach to the last accessed session
```sh
tmux attach 
```
- Exit and quit tmux
```sh
Ctrl+B &
```

### Session management
- Create a named session
```sh
tmux new -s <session_name>
```
- Attach to a session
```sh
tmux attach -t <session_name>
```
or
```sh
tmux a -t <session_name>
```
- Switch sessions
```sh
tmux switch -t <session_name>
```
- List sessions
```sh
tmux ls
```
- Kill sessions
```sh
tmux kill-session -t <session_name>
```
### Window management

| Key        | Operation                      |
| ---------- | ------------------------------ |
| Ctrl+B C   | Create new window              |
| Ctrl+B N   | Move to next window            |
| Ctrl+B P   | Move to previous window        |
| Ctrl+B L   | Move to last window            |
| Ctrl+B 0-9 | Move to window by index number |

### Pane management
| Key                    | Operation                                                    |
| ---------------------- | ------------------------------------------------------------ |
| Ctrl+B %               | Vertical split (panes side by side)                          |
| Ctrl+B "               | Horizontal split (one pane below the other)                  |
| Ctrl+B O               | Move to other pane                                           |
| Ctrl+B !               | Remove all panes but the current one from the window         |
| Ctrl+B Q               | Display window index numbers                                 |
| Ctrl+B Ctrl-Up/Down    | Resize current pane (due north/south)                        |
| Ctrl+B Ctrl-Left/Right | Resize current pane (due west/east)                          |
| Ctrl+B W               | Open a panel to navigate across windows in multiple sessions |
### Command mode
Some other key points are we can enter command mode by 

Ctrl + B :

We can allow access to a mouse by 

```sh
set -g mouse
```

### Tmux config

We have a tmux config file that is a dot config file. We can create it using the following command.
```sh
vi $HOME/.tmux.conf
```
We can add the above command to allow mouse usage into the tmux conf file.


### Tmux References

There is a lot more to tmux than this, which we can check out from the  [tmux manual](http://man.openbsd.org/OpenBSD-current/man1/tmux.1).