---
title: "Projects"
date: 2021-12-01T15:46:22Z
draft: false
showPagination: false
showAuthor: false
showDate: false
showReadingTime: false
showWordCount: false
sharingLinks: false
---

### Blog RAG

This project uses all open source and freely available resources to create a simple LangChain application. The basic idea is to take in few of my blog articles as the relevant context information and pass it onto a LLM to get grounded response from the LLM.

[Github Link](https://github.com/vigneshm243/blogRAG)
[Article Link](https://vignesh.page/posts/rag_using_langchain_langsmith/)

### Word Count in Go

I recently came across the [Coding Challenges](https://codingchallenges.fyi/challenges/challenge-wc) for a wc clone. So, I gave it a try.

[Github Link](https://github.com/vigneshm243/CodingChallenges/tree/main/wctool)


### Go Quotes API

The API just hits the Goodreads quotes page and fires a search. It returns a list of Quotes with their authors. I have built it and deployed it on Heroku.

[Github Link](https://github.com/vigneshm243/go-quotes-api)

### This site

This page has been built on Hugo and hosted on Gitlab. There is a gitlab pipeline running that rebuilds the site on pushing any new article as a markdown file.

[Gitlab Link](https://gitlab.com/vigneshm243/vigneshm)