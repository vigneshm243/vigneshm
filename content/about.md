---
title: "Vignesh Muthukumaran"
date: 2021-11-30T10:47:16Z
draft: false
showPagination: false
showAuthor: false
showDate: false
showReadingTime: false
showWordCount: false
sharingLinks: false
layout: "simple"
---


- I am a Senior Software Engineer for [Oracle](https://www.oracle.com/) in Chennai, India.

- I was a Software Engineer Trainee for Infosys at Mysore, India.

- I have since worked at Bangalore, Mumbai, Dubai and Chennai for various clients in the BFSI sector for Infosys as a Software Engineer/ Senior Software Engineer.

- I love to watch anime and have run through most of the popular ones. Top picks for me would be Death Note, Code Geass and Haikyuu. 

- I enjoy [reading](https://www.goodreads.com/vigneshm243) leisurely. I read a lot of science fiction, fantasy and historical fiction. I also do love some self help and non fiction.

- I am fascinated with music and I am a novice pianist.

- I aspire to learn a few languages in my life time. Currently fluent in Tamil and English. Dabbling in Japanese and Spanish.

- I like to play Chess in my free time and try to solve few chess puzzles daily.