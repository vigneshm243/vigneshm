---
title: "Welcome to vignesh.page!"
description: "Personal website where I write articles and share my projects"
---

{{<typeit tag=h3 speed=30  loop=true breakLines=false lifeLike=true >}}Senior Software Engineer
Hobbist Hacker
Budding Solutions Architect
Lifelong Learner{{</typeit>}}

